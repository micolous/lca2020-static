---
layout: post
title: "Keynote announcement: Lizzie O'Shea"
card: keynote_lizzie.98442f88.png
---

<p class="lead">Our second keynote speaker for LCA2020 is Lizzie O'Shea.</p>

<img class="img-fluid float-sm-right profile-pic" src="/media/img/lizzie_oshea.jpg" width="250">

Lizzie is a lawyer, writer, and broadcaster with a keen interest in Open Source.
She believes Open Source rebalances power in favour of the user by prioritising security by design, rather than imposing a requirement of trust in circumstances where users are not empowered.

Lizzie's keynote will be an open discussion of the encryption laws in Australia and an insight into, what she likes to call, the sausage making factory that is Canberra.
Delegates will gain an understanding of technological and digital rights issues are discussed in parliament and how introducing a Human Rights Act in Australia could have a positive impact in this space.

## A bit about Lizzie

Lizzie O'Shea is a lawyer, broadcaster, and writer.
She is a founder and board member of Digital Rights Watch, which advocates for human rights online.
Lizzie also sits on the board of the National Justice Project, Blueprint for Free Speech and the Alliance for Gambling Reform.
Lizzie's work has seen her receive the Davis Projects for Peace Prize and she has also been named a Human Rights Hero by Access Now.

## Interested?

If you’re interested to hear Lizzie speak make sure you register for linux.conf.au 2020.
You can purchase your [tickets](/attend/tickets/) today.

You can also become a [Digital Rights Watch Defender](http://digitalrightswatch.org.au/defenders/).
